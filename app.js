/**
 * Created by sandeepkumar on 02/03/16.
 */

angular.module("demoApp",["ngRoute"]).config(function($routeProvider){

    $routeProvider.when("/",{
        templateUrl:"home.html",
        controller:"homeCtr"
    }).when("/login",{
        templateUrl:"login.html",
        controller:"loginCtr"
    }).otherwise({
        redirectTo:"/"
    });

});
